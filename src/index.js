import {select, selectAll} from 'd3'

var testData = [
    {
        "id" : 1,
        "value": 1,
        "text": "one",
        "date": "2012-01-01"
    },
    {
        "id" : 2,
        "value": 2,
        "text": "two",
        "date": "2012-01-02"
    },
    {
        "id" : 3,
        "value": 3,
        "text": "three",
        "date": "2012-01-03"
    },
    {
        "id" : 4,
        "value": 4,
        "text": "four",
        "date": "2012-01-04"
    },
    {
        "id" : 6,
        "value": 6,
        "text": "six",
        "date": "2012-01-06"
    },
    {
        "id" : 5,
        "value": 5,
        "text": "five",
        "date": "2012-01-05"
    },
    {
        "id" : 1,
        "value": 1,
        "text": "one",
        "date": "2012-01-01"
    },
    {
        "id" : 2,
        "value": 2,
        "text": "two",
        "date": "2012-01-02"
    },
    {
        "id" : 3,
        "value": 3,
        "text": "three",
        "date": "2012-01-03"
    },
    {
        "id" : 4,
        "value": 4,
        "text": "four",
        "date": "2012-01-04"
    },
    {
        "id" : 6,
        "value": 6,
        "text": "six",
        "date": "2012-01-06"
    },
    {
        "id" : 5,
        "value": 5,
        "text": "five",
        "date": "2012-01-05"
    }
]


class CustomTable{
    constructor( target, opts ){
        this.target = select(target)
        this.width = "100%"
        this.height = "100%"
        this.data = []
        this.columns = []
    }

    draw(){
        var self = this
        var container = this.target
            .style("height", function(){
                if(self.height !== undefined){
                    return self.height
                } else{
                    return select(this).style("height")
                }
            })
            .style("overflow", "auto")
        

        var table = container.append("table")
            .style("width", this.width)
            .style("height", "inherit")
            .style("border-collapse", "collapse")
        
        var tableHead = table.append("thead")
        var tableBody = table.append("tbody")
        
        tableBody.selectAll(".data-row").remove()

        var rows = tableBody.selectAll("tr.data-row")
            .data(this.data).enter()
            .append("tr")
            .attr("class", "data-row")

        this.drawHead(tableHead)
        this.drawRows(rows)

        return this
    }

    drawHead( head ){
        this.columns.forEach(function(col){
            head.append("th")
                .text(col)
                .style("background-color", "white")
                .style("position", "sticky")
                .style("top", 0)
        })
    }

    drawRows( rows ){
        this.columns.forEach(function(col, i){
            rows.append("td")
                .text(d => d[col])
        })
    }

    bindData( raw_data ){
        this.data = raw_data
        this.columns = Object.keys(raw_data[0])
        return this
    }
}

var testTable = new CustomTable("#testDiv", {})
testTable.bindData(testData)
    .draw()