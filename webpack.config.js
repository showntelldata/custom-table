const path = require('path')
const webpack = require('webpack')

const paths = {
    src : path.join(__dirname, "src"),
    dist: path.join(__dirname, "dist"),
}

module.exports = {
    entry: {
        CustomTable : "./src/index.js",
    },
    output: {
        filename: "[name].js",
        path: paths.dist
    },
    devtool: "inline-source-map",
    devServer: {
        contentBase: paths.dist,
        compress: true,
        port: 8080
      },
    mode: "development",
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/
            },
            {
                test: /\.css$/,
                use: [
                    {loader: 'style-loader'},
                    {loader: 'css-loader'}
                ]
            }
        ]
    }
}